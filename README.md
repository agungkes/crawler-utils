# crawler-utils

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

Utilitas yang terdapat pada package ini adalah

- [p-retry](https://www.npmjs.com/package/p-retry)
- [p-queue](https://www.npmjs.com/package/p-queue)

## Why

Package ini dibuat agar library yang digunakan pada crawler dapat dengan mudah dilakukan perawatan (*maintenance*)

## How to use

```
const { PQueue, pRetry } = require('@agungkes/crawler-utils')
```

## Development

1. _Clone repository_ ini
2. Install library baru yang akan ditambahkan
3. Re-export library baru yang baru saja ditambahkan
4. Untuk mendapatkan dukungan penuh terhadap typescript pada editor **vscode**. Ketikkan perintah `yarn dlx @yarnpkg/pnpify --sdk vscode`
   
## How to commit

1. Pastikan perubahan sudah dimasukkan ke dalam git, jika belum ketikkan perintah `git add .` untuk memasukkan semua file atau `git add nama_file`
2. Sesuaikan pesan pada saat commit sesuai dengan **semantic release**

   | Commit message                                                                                                                                                                                   | Release type               |
   | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------- |
   | `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
   | `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
   | `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |
