interface Store {
  created_at: string;
  updated_at: string;
  id_marketplace: number;
  url: string;
  name: string;
  location: string;
  description: string;
  joined_since: string;
  last_update: string;
  seller_rate: string;
  seller_type: string;
  brand: string;
  crawled_from: string;
  total_product: number;
  target: number;
  followers: number;
}

interface Product {
  id_product: number;
  id_store: number;
  id_keywords: number;
  id_commodity: number;
  name: string;
  description: string;
  category: string;
  category_url: string;
  picture: string;
  screenshot: string;
  note: string;
  created_at: number;
  updated_at: number;
  url: string;
}

interface DataProduct {
  id_product: number;
  price: string;
  crawled: string;
  rating: number;
  discount: number;
  item_count: number;
  sold_count: number;
  view_count: number;
  favorited_count: number;
  processing_time: string;
  item_condition: string;
  keywords: string;
  manufacturer: string;
  created_at: number;
  updated_at: number;
}

export interface MessageParams {
  store: Partial<Store>;
  product: Partial<Product>;
  data_product: Partial<DataProduct>;
  url: string;
}
