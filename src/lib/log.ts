import TelegramBot from 'node-telegram-bot-api';

if (!process.env.TELEGRAM_TOKEN) {
  throw new Error('TELEGRAM_TOKEN must be filled in .env');
}

if (!process.env.TELEGRAM_CHAT_ID) {
  throw new Error('TELEGRAM_CHAT_ID must be filled in .env');
}

const bot = new TelegramBot(process.env.TELEGRAM_TOKEN);

const log = (message: string) => {
  bot.sendMessage(String(process.env.TELEGRAM_CHAT_ID), message);
};

export default log;
