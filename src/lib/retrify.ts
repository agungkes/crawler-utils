import delay from 'delay';
import pRetry from 'p-retry';

const retrify = async <T = void>(fn: () => T) => {
  const retry = await pRetry<T>(fn, {
    onFailedAttempt: async error => {
      console.log(
        `Attempt ${error.attemptNumber} failed. There are ${error.retriesLeft} retries left.`
      );
      console.log('Wait 10 second before running again');

      await delay(10000); // wait 10 second before running again
    },
    retries: 5,
  });

  return retry;
};

export default retrify;
