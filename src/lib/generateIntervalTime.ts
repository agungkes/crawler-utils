const generateIntervalTime = async () => {
  const dayjs = (await import('dayjs')).default;
  const createdAt = dayjs();
  const hour = createdAt.hour();
  const minute = createdAt.minute();

  return `${minute} ${hour} * * *`;
};

export default generateIntervalTime;
