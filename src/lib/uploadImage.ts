import fetch from 'node-fetch';
import fs from 'fs';
import FormData from 'form-data';

const API_ENDPOINT = {
  monitoring: '/api/product/',
  crawler: '/api/product/screenshot/',
};
const API_IMAGE_URL = process.env.API_IMAGE_URL || 'http://127.0.0.1:8000';
const uploadImage = async (
  imagePath: string,
  productId: string | number,
  crawler?: boolean
) => {
  try {
    const ENDPOINT_API = crawler
      ? API_ENDPOINT['crawler']
      : API_ENDPOINT['monitoring'];
    const formData = new FormData();
    formData.append('screenshot', fs.createReadStream(imagePath));

    await fetch(`${API_IMAGE_URL}${ENDPOINT_API}${productId}`, {
      method: 'POST',
      headers: formData.getHeaders(),
      body: formData,
    });
  } catch (error) {
    throw new Error(error.message);
  }
};

export default uploadImage;
