import redis from './redis';

const BROWSER_HOST = process.env.BROWSER_HOST || 'ws://localhost:3000';
const BROWSER_TOKEN = process.env.BROWSER_TOKEN || '';
const BROWSER_HEADLESS = process.env.APP_ENV === 'production' || false;

const BROWSER_WS_ENDPOINT = `${BROWSER_HOST}?token=${BROWSER_TOKEN}&headless=${BROWSER_HEADLESS}`;
export { BROWSER_WS_ENDPOINT, redis };
